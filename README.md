# SQLite3UniversalBinaries

### What It Does: ###

A simple magisk module to provide universal SQLite3 binaries

*Inspired by Olivier Pantale & the aarch64 module*

This module will first check to see if an SQLite3 binary already exists in the common paths (as per my GPay SQLite Fix module), 
so it doesnt try and install another or overwrite an existing one. 

Armeabi-v7a architecture and above is provided by this module

Specifically supported:

    armeabi-v7a
    arm64-v8a
    x86
    x86_64

This module will put an sqlite3 binary into **/system/bin** if  **/system/xbin** is not present

Any scripts relying on this sqlite3 binary need to check both paths for sqlite....

Script Example for testing for presence of an sqlite3 binary in common known paths (path stored in sqlpath)

``` # check where sqlite3 is
ui_print "Checking for sqlite3 binary...."
	if [ -f /data/data/com.termux/files/usr/bin/sqlite3 ] ; then
		sqlpath=/data/data/com.termux/files/usr/bin
		ui_print "[termux] SQLite3 binary found in: $sqlpath"
	elif [ -f /data/data/com.keramidas.TitaniumBackup/files/sqlite3 ] ; then
		sqlpath=data/data/com.keramidas.TitaniumBackup/files/sqlite3
		ui_print "[titanium] SQLite3 binary found in: $sqlpath"	
	elif [ -f /system/bin/sqlite3 ] ; then
		sqlpath=/system/bin
		ui_print "[standard] SQLite3 binary found in: $sqlpath"
	elif [ -f /system/xbin/sqlite3 ] ; then
		sqlpath=/system/xbin
		ui_print "[standard] SQLite3 binary found in: $sqlpath"
	else 
		ui_print "SQLite3 binary not found, please install an SQLite3 binary, without this the fix may not work"
		ui_print "Please read the original post (see support link) or /cache/playfixfirstrun.log after reboot for further info"
	fi 	
```

---

### Module Installation: ###

- Download from **[Releases](https://gitlab.com/adrian.m.miller/sqlite3universalbinaries/-/releases)**  
![](https://gitlab.com/adrian.m.miller/sqlite3universalbinaries/-/badges/release.svg)
- Install the module via Magisk app/Fox Magisk Module Manager/MRepo
- Reboot

---


